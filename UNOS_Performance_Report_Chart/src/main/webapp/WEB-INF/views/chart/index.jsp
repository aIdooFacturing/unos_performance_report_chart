<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<%-- <script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
 --%>
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.moonlight.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.moonlight.mobile.min.css" />
<script src="${ctxPath }/resources/js/jquery.min.js"></script> 
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	function getGroup(){
		var url = "${ctxPath}/getJigList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decodeURIComponent(data.jig) + "'>" + decodeURIComponent(data.jig) + "</option>"; 
				});
				
				$("#group").html(option);
				
				getDvcList();
			}
		});
	};
	
	
	function after(idxx){
		
		var url = "${ctxPath}/getTableData.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + " 23:59:59" + 
					"&shopId=" + shopId +
					"&jig=" + $("#group").val();
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
		for(i=1;i<=100;i++){
			eval("var arr"+i+"=new Object()");		
		} 
		var taS=idxx;
		var taE=idxx+11;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.tableData;
				var list=[];				
				
					/* 				int[] arr=new int[json.length];
					 */
					 				var arr0 = new Array();
					 				var arr1 = new Array();
									var arr2 = new Array();
									var arr3 = new Array();
									
									
									for(i=taS,ii=0;i<taE;i++){
										if(i>=json.length){
											arr0[ii]=" ";
											arr1[ii]=" ";
											arr2[ii]=" ";
											arr3[ii]=" ";
											ii++;
										}else{
											arr0[ii]=decode(json[i].name);					
											arr1[ii]=Number((json[i].inCycle_time/60/60).toFixed(1));
											arr2[ii]=Number((json[i].wait_time/60/60).toFixed(1));
											arr3[ii]=Number((json[i].alarm_time/60/60).toFixed(1));
											ii++;
										}
									}
									
									var series = [{
							            data: arr1,
							            //밝은녹색
							            //color: "#A3D800",
							            color: "#50ba29",
							            // Line chart marker type
							            markers: { type: "square" }
							        }, 
							        {
										data: arr2,
										//color: "#FF9100",
										color: "#ffff00"
							        },{
							            data: arr3,
							            color: "#C41C00",
							          }];

								        function createChart() {
								            $("#chart").kendoChart({
								                legend: {
								                    position: "bottom"
								                },
								                theme: "materialBlack",
								                seriesDefaults: {
								                    type: "column",
								                    stack: true
								                },
								                series: series,
								                chartArea: {
									                width: getElSize(3280),
								                	height: getElSize(590),
								                	margin: {
							                	      left: getElSize(100)
							                	    }
								                },
								                valueAxis: {
								                    line: {
								                        visible: false
								                    },
								                    majorUnit: 12
								                },
								                // X축
								                categoryAxis: {
								                    categories: arr0,
								                    majorGridLines: {
								                        visible: false
								                    } 
								                },
								                tooltip: {
								                    visible: true,
								                    format: "{0}"
								                }
								            });
								        }

								        $(document).ready(function() {
								            createChart();
								            $(document).bind("kendo:skinChange", createChart);
								            $(".options").bind("change", refresh);
								        });
								 		
								        function refresh() {
								            var chart = $("#chart").data("kendoChart"),
								                type = $("input[name=seriesType]:checked").val(),
								                stack = $("#stack").prop("checked");
								            for (var i = 0, length = series.length; i < length; i++) {
								                series[i].stack = stack;
								                series[i].type = type;
								            };
								            chart.setOptions({
								                series: series
								            });
								        }
								        
										$('.k-chart g text').css({
										    "font-size" : getElSize(35)
										}) 
			 	var stDate = new Date($("#sDate").val()) ;
			    var endDate = new Date($("#eDate").val()) ;
			 
			    var btMs = endDate.getTime() - stDate.getTime() ;
			    var btDay = btMs / (1000*60*60*24) ;
								    
				var day_cnt = btDay+1;
			}
		});
	}
	function getDvcList(){
		$('.k-header').remove();
		var url = "${ctxPath}/getTableData.do";

		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + " 23:59:59" + 
					"&shopId=" + shopId +
					"&jig=" + $("#group").val();
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
		for(i=1;i<=100;i++){
			eval("var arr"+i+"=new Object()");		
		} 
		var list=[];
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.tableData;
				
				var start = new Date(sDate);
				var end = new Date(eDate);
				var n = (end - start)/(24 * 3600 * 1000)+1;
				var jsonlength=Number(json.length);
				var size
				if(jsonlength%11>0){
					size = jsonlength/11
					size = size +1
				}else{
					size = jsonlength/11
				}
				var taStart=0;// no1첫밴째 열 추가하기위해 0을안
				var taEnd=11;						
				var tr = "";
				var arridx=1;
				
				for(i=1; i<=jsonlength; i++){
					eval("arr"+i+".no"+i+"=i");
				}
				
				for(idx=1; idx<=size; idx++){
					//1 
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						eval("arr"+arridx+".no1='${division}'"); 
						eval("arr"+arridx+".no"+ii+"=decode(json[i].name)");
						ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1;
					//2
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
					var inCycle=Number((json[i].inCycle_time/60/60).toFixed(1));
					eval("arr"+arridx+".no1='${ophour}'");
					eval("arr"+arridx+".no"+ii+"=inCycle");
					ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1;
					//3
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var wait=(json[i].wait_time/60/60).toFixed(1);
						eval("arr"+arridx+".no1='${wait}'");
						eval("arr"+arridx+".no"+ii+"=wait");
						ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1; 
					 //4
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var alarm=(json[i].alarm_time/60/60).toFixed(1);
						eval("arr"+arridx+".no1='${stop}'");
						eval("arr"+arridx+".no"+ii+"=alarm");
						ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1;
					//5  
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var incycle=Number((json[i].inCycle_time/60/60).toFixed(1));
						var wait=Number((json[i].wait_time/60/60).toFixed(1));
						var alarm=Number((json[i].alarm_time/60/60).toFixed(1));
						var noconn=(24*n-(incycle+wait+alarm)).toFixed(1);
						eval("arr"+arridx+".no1='${noconnection}'");
						eval("arr"+arridx+".no"+ii+"=noconn");
						ii++  
						} 
					}
					list.push(eval("arr"+arridx)); 
					arridx+=1;
					//6 평균
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var alarm=(json[i].alarm_time/60/60).toFixed(1);
						eval("arr"+arridx+".no1='${avrg}'");
						eval("arr"+arridx+".no"+ii+"='${avrg}'");
						ii++
						}
					}
					list.push(eval("arr"+arridx)); 
					arridx+=1;
					//7행
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						eval("arr"+arridx+".no1='${division}'"); 
						eval("arr"+arridx+".no"+ii+"=decode(json[i].name)");
						ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1; 
	
					//8행
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
					var inCycle=Number((json[i].inCycle_time/60/60/n).toFixed(1));
					eval("arr"+arridx+".no1='${ophour}'");
					eval("arr"+arridx+".no"+ii+"=inCycle");
					ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1; 
	
					//9행
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var wait=(json[i].wait_time/60/60/n).toFixed(1);
						eval("arr"+arridx+".no1='${wait}'");
						eval("arr"+arridx+".no"+ii+"=wait");
						ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1; 
					
					//10행
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var alarm=(json[i].alarm_time/60/60/n).toFixed(1);
						eval("arr"+arridx+".no1='${stop}'");
						eval("arr"+arridx+".no"+ii+"=alarm");
						ii++
						}
					}
					list.push(eval("arr"+arridx));
					arridx+=1;
					
					//11행
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						var incycle=Number((json[i].inCycle_time/60/60/n).toFixed(1));
						var wait=Number((json[i].wait_time/60/60/n).toFixed(1));
						var alarm=Number((json[i].alarm_time/60/60/n).toFixed(1));
						var noconn=(24-(incycle+wait+alarm)).toFixed(1);
						eval("arr"+arridx+".no1='${noconnection}'");
						eval("arr"+arridx+".no"+ii+"=noconn");
						ii++  
						} 
					}
					list.push(eval("arr"+arridx)); 
					arridx+=1;
					
					//11행
					for(i=taStart,ii=2; i<taEnd; i++){
						if(i==jsonlength){
							break;
						}else{
						eval("arr"+arridx+".no1='생산수량'");
						eval("arr"+arridx+".no"+ii+"=json[i].production");
						ii++  
						} 
					}
					list.push(eval("arr"+arridx)); 
					arridx+=1;
					taStart+=11; 
					taEnd+=11;
				}//for				
				
				console.log("list")
				console.log(list)
				
				$("#table2").kendoGrid({				 	
			 		dataSource: list,
					height: getElSize(1000),
			 		pageable:{
						pageSize: 12,
						info: false,
						buttonCount:5,
						change: function(e){
							if(e.index==1){
								after(((e.index-1)*10));
							}else{
								after(((e.index-1)*10)+(e.index-1));
							}
							
/* 							$('#nowpage').html(e.index+" / "+Math.floor(json.length/10+1));
 */
 $("td:contains('생산수량'):last").css({
		"background-color" :  "#3496F6",
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
	 
	 
	$("tr.k-alt:contains('${avrg}')").css({
		"background-color" :  "#A6A6A6",
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
	
	$("td[role='gridcell']:contains('${ophour}')").css({
		//"background-color" :  "#A3D800",
		"background-color" :  "#50ba29",
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
	
	$("td[role='gridcell']:contains('${wait}')").css({
		//"background-color" :  "#FF9100",
		"background-color" :  "#ffff00",
		
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
	
	$("td[role='gridcell']:contains('${stop}')").css({
		"background-color" :  "#C41C00",
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
	
	$("td[role='gridcell']:contains('${noconnection}')").css({
		"background-color" :  "#6B6C7C",
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
	
	$("td[role='gridcell']:contains('${division}')").css({
		"background-color" :  "#A6A6A6",
		"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
	});	
			 				
			 				/* $('.k-header').css({
								"text-align" : "center",
								"font-size" : getElSize(20)
							})
							
							$(".k-grid-content table tbody").css({
								"text-align" : "center",
								"font-size" : getElSize(20)
							})
							$('.k-grid').css({
							    "font-size": getElSize(20)
							})
							$('.k-pager-numbers').css({
								"font-size": getElSize(20)
							})
							$('#table2').css({
								"text-align" : "center",
								"font-size" : getElSize(20)
							})  */
			            }
					},
		            columns:[{
		            		field:"no1",title:"  ",width:getElSize(180),attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no2",title:" ",width:getElSize(280),attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no3",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no4",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no5",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no6",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no7",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no8",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no9",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no10",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no11",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no12",title:" ",attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30)+ "; color : white"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    }
	       		    ]
				}); 

				//chart
				chart(data);
				function chart(data){
					/* 				int[] arr=new int[json.length];
					 */
					 				var arr0 = new Array();
					 				var arr1 = new Array();
									var arr2 = new Array();
									var arr3 = new Array();
									for(i=0;i<11;i++){
										if(i>=json.length){
											arr0[i]=" ";
											arr1[i]=" ";
											arr2[i]=" ";
											arr3[i]=" ";
										}else{
											arr0[i]=decode(json[i].name);											
											arr1[i]=Number((json[i].inCycle_time/60/60).toFixed(1));											
											arr2[i]=Number((json[i].wait_time/60/60).toFixed(1));
											arr3[i]=Number((json[i].alarm_time/60/60).toFixed(1));
										}
									}
									
									var series = [{
								            data: arr1,
								          	//초록색
								            //color: "#A3D800",
								            color: "#50ba29",
								            // Line chart marker type
								            markers: { type: "square" },
								        }, 
								        {
											data: arr2,
											//color: "#FF9100"
											color: "#ffff00"
												
								        },{
								            data: arr3,
								            color: "#C41C00"
								          }];
									
								        function createChart() {
								            $("#chart").kendoChart({								                
								                legend: {
								                    position: "bottom"
								                },
								                theme: "materialBlack",
								                seriesDefaults: {
								                    type: "column",
								                    stack: true, 
								                    labels: {
								                        visible: false,
								                        position: "bottom",
								                        background: "transparent",
								                        
								                    }
								                },
								                series: series,
								                chartArea: {
									                width: getElSize(3280),
								                	height: getElSize(590),
								                	margin: {
							                	      left: getElSize(100)
							                	    }
								                },
								                valueAxis: {
								                    line: {
								                        visible: false
								                    },
								                    majorUnit: 12
								                },
								                // X축
								                categoryAxis: {
								                    categories: arr0,
								                    majorGridLines: {
								                        visible: false
								                    } 
								                },
								                tooltip: {
								                    visible: true,
								                    format: "{0}"
								                }
								            });
								            
								        }

								        
								        
								        
								        $(document).ready(function() {
								            createChart();
								            $(document).bind("kendo:skinChange", createChart);
								            $(".options").bind("change", refresh);
								        });
								 		
								        function refresh() {
								            var chart = $("#chart").data("kendoChart"),
								                type = $("input[name=seriesType]:checked").val(),
								                stack = $("#stack").prop("checked");
								            for (var i = 0, length = series.length; i < length; i++) {
								                series[i].stack = stack;
								                series[i].type = type;
								            };
								            chart.setOptions({
								                series: series,
								            });
								        }
									}//chart		
		
			 	var stDate = new Date($("#sDate").val()) ;
			    var endDate = new Date($("#eDate").val()) ;
			 
			    var btMs = endDate.getTime() - stDate.getTime() ;
			    var btDay = btMs / (1000*60*60*24) ;
								    
				var day_cnt = btDay+1;

				
				$('.k-pager-nav').hide();

				$('.k-chart g text').css({
				    "font-size" : getElSize(35)
				}) 
				
/* 				$('.k-pager-wrap.k-grid-pager.k-widget.k-floatwrap').css({
					"font-size" : getElSize(35),
					"margin-top" : getElSize(35),
					margin-top:20px; !important;
		            height:80px; !important;
				})
 
				//setEl();
/* 				$('#nowpage').html("1 / "+Math.floor(json.length/10+1));
 				$("#arrow_right").click(function(){
 					idxx=Number($('.k-state-selected').html());
 					idxx=idxx*10;
 					if(idxx<json.length){
	 					after(idxx);
	 					$('.k-link').eq(6).trigger('click');
 					}else{
 						alert("${end_of_chart}");
 					}
					$('#nowpage').html(+" / "+json.length/10)
			});
 				
 				$("#arrow_left").click(function(){
 					idxx=Number($('.k-state-selected').html());
 					idxx=(idxx-2)*10;
 					if(idxx>=0){
	 					after(idxx);
	 					$('.k-link').eq(1).trigger('click');
 					}else{
 						alert("${first_of_chart}");
 					}
				});	
 				
 				 $('#nowpage').css({
					"color" : "white"
				})  */
//dd				
 /* 				$('.k-pager-numbers').css({
					"font-size": getElSize(20)
				})
				
				$('#content_table td').css({
				    "font-size": getElSize(20)
				})
				$('.k-link').css({
					"font-size": getElSize(20)
				})
				$('.k-pager-info.k-label').css({
					"font-size": getElSize(20)
				}) */
//
				/*
				$('#content_table tbody tr').css({
				    "height":getElSize(5)
				})
				$('.date').css({
					"font-size" : getElSize(35),
					"height" :getElSize(60)
				})
				$('#zzz').css({
				    "height":getElSize(40),
				    "font-size" : getElSize(35)
				})
 				 */
				
				/* $(".k-grid-content table tbody").css({
					"text-align" : "center",
					"font-size" : getElSize(35)
				}) */
				
/* 				$('.k-grid-header-wrap thead').css({
				    "font-size": getElSize(80),
				    "height":getElSize(80)
				})
 */				/* $('.k-pager-numbers').css({
					"font-size": getElSize(20)
				})
				$('.k-grid').css({
				    "font-size": getElSize(20)
				})
				//chart
				$('.k-chart g text').css({
				    "font-size" : getElSize(40)
				}) */
				/* $('#table2.k-grid.k-widget.k-display-block').css({
				    "height":getElSize(1030)
				}) */

				/*
				$('.k-grid').css({
							    "font-size": getElSize(20)
				})
				
				$('#table2').css({
					"text-align" : "center",
					"font-size" : getElSize(20)
				}) */
				
				$("td:contains('생산수량'):last").css({
					"background-color" :  "#3496F6",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
 				 
 				 
				$("tr.k-alt:contains('${avrg}')").css({
					"background-color" :  "#A6A6A6",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
 				$("td[role='gridcell']:contains('${ophour}')").css({
					//"background-color" :  "#A3D800",
					"background-color" :  "#50ba29",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
 				
 				$("td[role='gridcell']:contains('${wait}')").css({
					//"background-color" :  "#FF9100",
					"background-color" :  "#ffff00",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
 				
				$("td[role='gridcell']:contains('${stop}')").css({
					"background-color" :  "#C41C00",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
				$("td[role='gridcell']:contains('${noconnection}')").css({
					"background-color" :  "#6B6C7C",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
				$("td[role='gridcell']:contains('${division}')").css({
					"background-color" :  "#A6A6A6",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});					
				
			//	$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
			}//success
		});
	};

	var wcDataList = new Array();
	var dateList = new Array();
	var wcList = new Array();
	var wcName = new Array();
	var maxPage;
	var cBarPoint = 0;

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#sDate").val(caldate(7));
		$("#eDate").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 10 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 10 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;
	$(function(){
		createNav("analysis_nav",0);
		getGroup();
		setDate();	
		time();
		
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
				
		$("img").css({
			"display" : "inline"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#group").css({
			"height" : getElSize(80)
		})
		
		$("#zzz").css({
			"color" : "white"
		})
		
		if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/performanceReport_table.do"
	};
	var color = "";
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
<script>
var idxx=0;

</script>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="dashBoard_title">${operation_chart_title}</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none" >
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left' style="display: none"  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right' style="display: none" >
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'><spring:message code="performance_chart"></spring:message></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left nav' style="display: none" >
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td id="zzz"><select id="group"></select>
								<spring:message code="op_period"></spring:message> <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate"> 
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getDvcList()">
								<button onclick="goGraph()" id="table"><spring:message code="table"></spring:message> </button>
							</td>
						</Tr>		
						<tr>
							<td>
						
								
							<!-- 	<div id="tableContainer" width="100%">
								</div> -->
								<div id="chart" style="width: 100%">
								</div>
								<div id='table2'></div>
								<div id="tableContainer_avg" width="100%" >
								</div>
								
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>	
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/selected_green.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none"  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none"  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none"  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'style="display: none"   >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>